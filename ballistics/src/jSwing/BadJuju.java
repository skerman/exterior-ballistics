package jSwing;

import java.awt.EventQueue;

import javax.swing.JFrame;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.border.BevelBorder;

public class BadJuju {

	private JFrame frmBadJujuBallistics;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JComboBox<Object> textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	
	private JLabel lblLoadName;
	private JLabel lblLoadDesc;
	private JLabel lblMuzzleVelocity;
	private JLabel lblBullet;
	private JLabel lblWt;
	private JLabel lblBC;
	private JLabel lblModel;
	private JLabel lblTargetRng;
	private JLabel lblTargetElevation;
	private JLabel lblHighlow;
	private JLabel lblScopeHeight;
	private JLabel lblZeroRange;
	private JLabel lbllblZeroAngle;	
	private JLabel lblTemp;
	private JLabel lblPressure;
	private JLabel lblRelHum;
	private JLabel lblAltitude;
	private JLabel lblWindSpeed;
	private JLabel lblWindHeading;
	
	
	private JLabel lblLoad;
	private JLabel lblLoad_1;
	private JLabel lblLoad_2;
	private JLabel lblNewLabel_10;
	private JLabel label;
	private JLabel label_1;
	
	private JButton button;
	private JButton btnRun;
	private JToggleButton toggleButton;
	private JToggleButton toggleButton_1;
	private JToggleButton toggleButton_2;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BadJuju window = new BadJuju();
					window.frmBadJujuBallistics.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BadJuju() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBadJujuBallistics = new JFrame();
		frmBadJujuBallistics.setTitle("Bad Juju Ballistics");
		frmBadJujuBallistics.setBounds(100, 100, 914, 810);
		frmBadJujuBallistics.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBadJujuBallistics.getContentPane().setLayout(new MigLayout("", "[65.00,grow][85.00,grow][57.00,grow][58.00,grow][grow][99.00,grow][grow][grow][99.00,grow][grow][grow][97.00,grow][grow][grow][grow][][grow][][grow]", "[][][][][][][][][][][][][][][][][][][][][][][]"));
		
		lblLoad = new JLabel("Load 1");
		lblLoad.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblLoad, "cell 5 0");
		
		lblLoad_1 = new JLabel("Load 2");
		lblLoad_1.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblLoad_1, "cell 8 0");
		
		lblLoad_2 = new JLabel("Load 3");
		lblLoad_2.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblLoad_2, "cell 11 0");
		
		textField = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField, "cell 0 1,alignx left");
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Load name");
		lblNewLabel.setLabelFor(textField);
		lblNewLabel.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel, "cell 1 1");
		
		lblNewLabel_10 = new JLabel("New label");
		lblNewLabel_10.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_10, "cell 5 1");
		
		textField_1 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_1, "cell 0 2,alignx left");
		textField_1.setColumns(10);
				
		JLabel lblNewLabel_1 = new JLabel("Load description");
		lblNewLabel_1.setLabelFor(textField_1);
		lblNewLabel_1.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_1, "cell 1 2,alignx left");
		
		label = new JLabel("New label");
		label.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		frmBadJujuBallistics.getContentPane().add(label, "cell 5 2");
		
		textField_2 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_2, "cell 0 3,alignx left");
		textField_2.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Muzzle velocity (fps)");
		lblNewLabel_2.setLabelFor(textField_2);
		lblNewLabel_2.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_2, "cell 1 3");
		
		label_1 = new JLabel("New label");
		label_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		frmBadJujuBallistics.getContentPane().add(label_1, "cell 5 3");
		
		textField_3 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_3, "cell 0 4,alignx left");
		textField_3.setColumns(10);
				
		JLabel lblNewLabel_3 = new JLabel("Bullet");
		lblNewLabel_3.setLabelFor(textField_3);
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_3, "cell 1 4");
		
		textField_4 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_4, "cell 0 5,alignx left");
		textField_4.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Weight (grains)");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_4, "cell 1 5");
		lblNewLabel_4.setLabelFor(textField_4);
		
		textField_5 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_5, "cell 0 6,alignx left");
		textField_5.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("BC");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNewLabel_5.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_5, "cell 1 6");
		lblNewLabel_5.setLabelFor(textField_5);
		
		textField_6 = new JComboBox<Object>();
		textField_6.setModel(new DefaultComboBoxModel<Object>(new String[] {"G1", "G7"}));
		frmBadJujuBallistics.getContentPane().add(textField_6, "cell 0 7,growx");
		
		JLabel lblNewLabel_6 = new JLabel("Model");
		lblNewLabel_6.setLabelFor(textField_6);
		lblNewLabel_6.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_6, "cell 1 7");
		
		textField_10 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_10, "cell 0 8,growx");
		textField_10.setColumns(10);
		
		lblTargetRng = new JLabel("Target range (yds.)");
		lblTargetRng.setLabelFor(textField_10);
		lblTargetRng.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblTargetRng, "cell 1 8");
		
		textField_11 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_11, "cell 0 9,growx");
		textField_11.setColumns(10);
		
		lblTargetElevation = new JLabel("Target elevation (+/- feet)");
		lblTargetElevation.setLabelFor(textField_11);
		lblTargetElevation.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblTargetElevation, "cell 1 9");
		
		textField_7 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_7, "cell 0 10,growx");
		textField_7.setColumns(10);
		
		lblHighlow = new JLabel("High/Low (+/- inches)");
		lblHighlow.setLabelFor(textField_7);
		lblHighlow.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblHighlow, "cell 1 10");
		
		textField_8 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_8, "cell 0 11,growx");
		textField_8.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Scope height (inches)");
		lblNewLabel_7.setLabelFor(textField_8);
		lblNewLabel_7.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_7, "cell 1 11");
		
		textField_12 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_12, "cell 0 12,growx");
		textField_12.setColumns(10);
		
		JLabel lblNewLabel_8 = new JLabel("Zero range (yds.)");
		lblNewLabel_8.setLabelFor(textField_12);
		lblNewLabel_8.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_8, "cell 1 12,alignx leading");
		
		textField_13 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_13, "cell 0 13,growx");
		textField_13.setColumns(10);
		
		JLabel lblNewLabel_9 = new JLabel("Zero angle (MOA)");
		lblNewLabel_9.setLabelFor(textField_13);
		lblNewLabel_9.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblNewLabel_9, "cell 1 13");
		
		textField_14 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_14, "cell 0 14,growx");
		textField_14.setColumns(10);
		
		lblTemp = new JLabel("Temp. (F)");
		lblTemp.setLabelFor(textField_14);
		lblTemp.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblTemp, "cell 1 14");
		
		textField_15 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_15, "cell 0 15,growx");
		textField_15.setColumns(10);
		
		lblPressure = new JLabel("Pressure (inHg)");
		lblPressure.setLabelFor(textField_15);
		lblPressure.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblPressure, "cell 1 15");
		
		textField_17 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_17, "cell 0 16,growx");
		textField_17.setColumns(10);
		
		lblRelHum = new JLabel("Rel. Hum. (%)");
		lblRelHum.setLabelFor(textField_17);
		lblRelHum.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblRelHum, "cell 1 16");
		
		textField_18 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_18, "cell 0 17,growx");
		textField_18.setColumns(10);
		
		lblAltitude = new JLabel("Altitude (ft.)");
		lblAltitude.setLabelFor(textField_18);
		lblAltitude.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblAltitude, "cell 1 17");
		
		textField_9 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_9, "cell 0 18,growx");
		textField_9.setColumns(10);
		
		lblWindSpeed = new JLabel("Wind speed (mph)");
		lblWindSpeed.setLabelFor(textField_9);
		lblWindSpeed.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblWindSpeed, "cell 1 18");
		
		textField_16 = new JTextField();
		frmBadJujuBallistics.getContentPane().add(textField_16, "cell 0 19,growx");
		textField_16.setColumns(10);
		
		lblWindHeading = new JLabel("Wind heading (degrees (N=0))");
		lblWindHeading.setLabelFor(textField_16);
		lblWindHeading.setFont(new Font("Cambria", Font.ITALIC, 13));
		frmBadJujuBallistics.getContentPane().add(lblWindHeading, "cell 1 19");
		
		button = new JButton("+");
		button.setFont(new Font("Cambria", Font.BOLD, 13));
		frmBadJujuBallistics.getContentPane().add(button, "cell 0 20,alignx center");
		
		toggleButton = new JToggleButton("");
		frmBadJujuBallistics.getContentPane().add(toggleButton, "cell 5 20,alignx center");
		
		toggleButton_1 = new JToggleButton("");
		frmBadJujuBallistics.getContentPane().add(toggleButton_1, "cell 8 20,alignx center");
		
		toggleButton_2 = new JToggleButton("");
		frmBadJujuBallistics.getContentPane().add(toggleButton_2, "cell 11 20");
		
		btnRun = new JButton("Run");
		frmBadJujuBallistics.getContentPane().add(btnRun, "cell 16 22");
	}

}
