package ballistics
import swing._
import event._
import java.awt.GridBagConstraints
import GridBagPanel._
import java.awt.Font

object BadJuju extends SimpleSwingApplication {
  val loads = List[Trajectory]()
  def top = new MainFrame {
    title = "Bad Juju Ballistics"
    val run = new Button {text="Run"}
    val add1 = new Button {text="+"}
    val tglButton = new ToggleButton("plot")
	val	tglButton_1 = new ToggleButton("plot")
	val tglButton_2 = new ToggleButton("plot")
    
    // column headings, if you will, for the three loads to be plotted
    object lblLoadOne extends Label{
      text="Load One"
      font = new Font("Cambria", Font.ITALIC, 13)
      }
  
    object lblLoadTwo extends Label{
      text="Load Two"
      font = new Font("Cambria", Font.ITALIC, 13)
      }
        
    object lblLoadThree extends Label{
      text="Load Three"
      font = new Font("Cambria", Font.ITALIC, 13)
      }
    
    //Labels for things
    object lblLoadName extends Label{
      text="Load Name "
      font = new Font("Cambria", Font.ITALIC, 13)
      xAlignment = Alignment.Right}
    
    object lblLoadDesc extends Label{
      text="Load description "
      font = new Font("Cambria", Font.ITALIC, 13)
      xAlignment = Alignment.Right}
    
	object lblMuzzleVelocity extends Label{
	  text="Muzzle velocity (fps) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblBullet extends Label{
	  text="Bullet "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblWt extends Label{
	  text="Weight (grains) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblBC extends Label{
	  text="BC "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblModel extends Label{
	  text="Model "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblTargetRng extends Label{
	  text="Target range (yds.) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblTargetElevation extends Label{
	  text="Target elevation (+/- feet) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblHighlow extends Label{
	  text="High/Low (+/- inches) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblScopeHeight extends Label{
	  text="Scope height (inches) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblZeroRange extends Label{
	  text="Zero range (yds.) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblZeroAngle extends Label{
	  text="Zero angle (MOA) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblTemp extends Label{
	  text="Temp. (F) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblPressure extends Label{
	  text="Pressure (inHg) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblRelHum extends Label{
	  text="Rel. Hum. (%) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblAltitude extends Label{
	  text="Altitude (ft.) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblWindSpeed extends Label{
	  text="Wind speed (mph) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
	
	object lblWindHeading extends Label{
	  text="Wind heading (deg) "
	  font = new Font("Cambria", Font.ITALIC, 13)
	  xAlignment = Alignment.Right}
    
    
    //things    
    
    object lname1 extends TextField  			// User assigned name for this load.
    object ldesc1 extends TextField 			// User assigned description for this load.
    object velo1 extends TextField  			// Initial velocity, in ft/s.
    object bname1 extends TextField 			// User assigned name for the bullet.
    object wt1 extends TextField 				// Bullet wt in grains.
    object bc1 extends TextField 				// BC of bullet for specified Drag Function.
    object model1 extends ComboBox(Seq("G1", "G7"))	// Drag Function: G1 or G7
    object trgtRng1 extends TextField 			// The target range in yds.
    object trgtElev1 extends TextField  		// The shooting angle (uphill / down hill), in degrees.
    object hilo1 extends TextField  			// How many inches above/below the zero.
    object sh1 extends TextField 				// The Sight height over bore, in inches.
    object zeroRng1 extends TextField 			// The zero range of the rifle, in yards.
    object zAngle1 extends TextField 			// Angle of barrel in MOA relative to horizontal to zero the rifle.
    object temp1 extends TextField 				// Ambient temperature in deg. Farenheit.
    object press1 extends TextField 			// Ambient barometric pressure in inchHg.
    object relHum1 extends TextField 			// Ambient relative humidity in %.
    object alt1 extends TextField 				// Local altitude in ft.
    object windSpeed1 extends TextField 		// The wind speed in miles per hour.
    object windHeading1 extends TextField		// The wind angle (0=head wind, 90=right to left, 180=tail wind, 270/-90=left to right).
   
    lazy val ui = new GridBagPanel {  
        val c = new Constraints
        val shouldFill = true
	    if (shouldFill) {
	      c.fill = Fill.Horizontal
	    }
        
        //labels
        c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 1
	    layout(lblLoadName) = c
	    
        c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 2
	    layout(lblLoadDesc) = c	    
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 3
	    layout(lblMuzzleVelocity) = c
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 4
	    layout(lblBullet) = c
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 5
	    layout(lblWt) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 6
	    layout(lblBC) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 7
	    layout(lblModel) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 8
	    layout(lblTargetRng) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 9
	    layout(lblTargetElevation) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 10
	    layout(lblHighlow) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 11
	    layout(lblScopeHeight) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 12
	    layout(lblZeroRange) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 13
	    layout(lblZeroAngle) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 14
	    layout(lblTemp) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 15
	    layout(lblPressure) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 16
	    layout(lblRelHum) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 17
	    layout(lblAltitude) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 18
	    layout(lblWindSpeed) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 0
	    c.gridy = 19
	    layout(lblWindHeading) = c	
	    
	    //text boxes Load 1
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 0
	    layout(lblLoadOne) = c
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 1
	    layout(lname1) = c
	    
        c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 2
	    layout(ldesc1) = c	    
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 3
	    layout(velo1) = c
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 4
	    layout(bname1) = c
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 5
	    layout(wt1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 6
	    layout(bc1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 7
	    layout(model1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 8
	    layout(trgtRng1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 9
	    layout(trgtElev1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 10
	    layout(hilo1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 11
	    layout(sh1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 12
	    layout(zeroRng1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 13
	    layout(zAngle1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 14
	    layout(temp1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 15
	    layout(press1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 16
	    layout(relHum1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 17
	    layout(alt1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 18
	    layout(windSpeed1) = c	
	    
	    c.weightx = 0.5
	    c.fill = Fill.Horizontal
	    c.gridx = 5
	    c.gridy = 19
	    layout(windHeading1) = c
	    
	    c.gridx = 5
	    c.gridy = 21
	    layout(add1)=c
	    
	    c.gridx = 5
	    c.gridy = 25
	    layout(tglButton)=c
        
        c.gridx = 50
	    c.gridy = 25
	    layout(tglButton_1)=c
        
        c.gridx = 100
	    c.gridy = 25
	    layout(tglButton_2)=c
	    
	    c.gridx = 200
	    c.gridy = 21
	    layout(run)=c
	    
	}
      contents = ui
      centerOnScreen  
      listenTo(run)
      listenTo(add1)
      reactions += {
        case ButtonClicked(b)=>
        	b match {
        	case `add1` => {
        	   new Trajectory(model1.item.toInt,
        					  velo1.text.toDouble,
        					  bc1.text.toDouble,
        					  zeroRng1.text.toDouble,
        					  sh1.text.toDouble,
        					  trgtElev1.text.toDouble,
        					  Some(windSpeed1.text.toDouble),
        					  Some(windHeading1.text.toDouble),
        					  Some(temp1.text.toDouble),
        					  Some(press1.text.toDouble),
        					  Some(relHum1.text.toDouble),
        					  Some(alt1.text.toDouble))::loads
        					  
        	    loads.head.zAngle = if (zAngle1.text.toDouble <=0) loads.head.ZeroAngle else zAngle1.text.toDouble
        	    }
        	case `run` => loads.foreach(_.solve)
        	}
      }
  }
     	
}