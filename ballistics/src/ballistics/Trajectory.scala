package ballistics
import scala.math._

class Trajectory (dragFunction:Int,dragCoefficient:Double,vi:Double, zRange:Double,sightHeight:Double,shootingAngle:Double, windSpeed:Option[Double], windAngle:Option[Double], temperature:Option[Double], pressure:Option[Double], relHumidity:Option[Double], altitude:Option[Double]) {
  val Gravity= -32.194
  val PI = 3.141592653589793
  var zAngle:Double=0
  val atmCorrect=AtmCorrect
   
  def retard(vi:Double):Double = {
   
    def g1:(Double,Double) = {
      if (vi>4230.0) (1.477404177730177e-04,1.9565)
      else if(vi>3680.0) (1.920339268755614e-04, 1.925)
      else if(vi>3450.0) (2.894751026819746e-04, 1.875)
      else if(vi>3295.0) (4.349905111115636e-04, 1.825)
      else if(vi>3130.0) (6.520421871892662e-04, 1.775)
      else if(vi>2960.0) (9.748073694078696e-04, 1.725)
      else if(vi>2830.0) (1.453721560187286e-03, 1.675)
      else if(vi>2680.0) (2.162887202930376e-03, 1.625)
      else if(vi>2460.0) (3.209559783129881e-03, 1.575)
      else if(vi>2225.0) (3.904368218691249e-03, 1.55)  
      else if(vi>2015.0) (3.222942271262336e-03, 1.575) 
      else if(vi>1890.0) (2.203329542297809e-03, 1.625) 
      else if(vi>1810.0) (1.511001028891904e-03, 1.675) 
      else if(vi>1730.0) (8.609957592468259e-04, 1.75)
      else if(vi>1595.0) (4.086146797305117e-04, 1.85)
      else if(vi>1520.0) (1.954473210037398e-04, 1.95)
      else if(vi>1420.0) (5.431896266462351e-05, 2.125)
      else if(vi>1360.0) (8.847742581674416e-06, 2.375)
      else if(vi>1315.0) (1.456922328720298e-06, 2.625) 
      else if(vi>1280.0) (2.419485191895565e-07, 2.875)
      else if(vi>1220.0) (1.657956321067612e-08, 3.25)
      else if(vi>1185.0) (4.745469537157371e-10, 3.75)
      else if(vi>1150.0) (1.379746590025088e-11, 4.25)
      else if(vi>1100.0) (4.070157961147882e-13, 4.75)
      else if(vi>1060.0) (2.938236954847331e-14, 5.125)
      else if(vi>1025.0) (1.228597370774746e-14, 5.25)
      else if(vi>980.0) (2.916938264100495e-14, 5.125)
      else if(vi>945.0) (3.855099424807451e-13, 4.75)
      else if(vi>905.0) (1.185097045689854e-11, 4.25)
      else if(vi>860.0) (3.566129470974951e-10, 3.75)
      else if(vi>810.0) (1.045513263966272e-08, 3.25)
      else if(vi>780.0) (1.291159200846216e-07, 2.875)
      else if(vi>750.0) (6.824429329105383e-07, 2.625)
      else if(vi>700.0) (3.569169672385163e-06, 2.375)
      else if(vi>640.0) (1.839015095899579e-05, 2.125)
      else if(vi>600.0) (5.71117468873424e-05,  1.950)
      else if(vi>550.0) (9.226557091973427e-05, 1.875)
      else if(vi>250.0) (9.337991957131389e-05, 1.875)
      else if(vi>100.0) (7.225247327590413e-05, 1.925)
      else if(vi>65.0) (5.792684957074546e-05, 1.975)
      else (5.206214107320588e-05, 2.000)
    } 			    
    def g7:(Double,Double) = {
    	if(vi>4200.0)(1.29081656775919e-09,3.24121295355962)
    	else if(vi>3000.0) (0.0171422231434847, 1.27907168025204)
    	else if(vi>1470.0) (2.33355948302505e-03, 1.52693913274526)
    	else if(vi>1260.0) (7.97592111627665e-04, 1.67688974440324)
    	else if(vi>1110.0) (5.71086414289273e-12, 4.3212826264889)
    	else if(vi>960.0) (3.02865108244904e-17, 5.99074203776707)
    	else if(vi>670.0) (7.52285155782535e-06, 2.1738019851075)
    	else if(vi>540.0) (1.31766281225189e-05, 2.08774690257991)
    	else (1.34504843776525e-05, 2.08702306738884)
    }
    			   
    dragFunction match {
      case 1 => g1._1*pow(vi,g1._2)/dragCoefficient*atmCorrect;
      case 7 => g7._1*pow(vi,g7._2)/dragCoefficient*atmCorrect;
    }
  }
  
 
  def solve={
      
    val headwind=HeadWind
    val crosswind=CrossWind
    val gy=Gravity*cos(toRadians((shootingAngle + zAngle)))
	val gx=Gravity*sin(toRadians((shootingAngle + zAngle)))
	
    
    def loop(vx:Double,vy:Double,t:Double,dt:Double,x:Double,y:Double):Unit ={
      println(f"$t%3.4f,${x/3}%3.6f,${y*12}%3.6f") //x in yds and y in inches
      if (abs(vy)>abs(3*vx)) Unit
      else if (t>1.25) Unit
      else {
    	var v=pow(pow(vx,2)+pow(vy,2),0.5)
		var dv=retard(v+headwind)
		var dvy = -dv*vy/v*dt
		var	dvx = -dv*vx/v*dt
		
		var	vy1=vy+dvy+dt*gy
		var	vx1=vx+dvx+dt*gx
			
		loop(vx1,vy1,t+1/v,1/v,x+dt*(vx+vx1)/2,y+dt*(vy+vy1)/2)
      }
    }
 	
	val vx=vi*cos(toRadians(zAngle))
	val vy=vi*sin(toRadians(zAngle))
	
	var dt=0.5/vi
    
    loop(vx,vy,0,dt,0,-sightHeight/12)
  }
  def ZeroAngle = {
    // The general idea here is to start at 0 degrees elevation, and increase the elevation by 14 degrees
    // until we are above the correct elevation.  Then reduce the angular change by half, and begin reducing
    // the angle.  Once we are again below the correct angle, reduce the angular change by half again, and go
    // back up.  This allows for a fast successive approximation of the correct elevation, usually within less
    // than 20 iterations.

    // theta: The actual angle of the bore.
    // da:  The change in the bore angle used to iterate in on the correct zero angle.
    // we'll start with a very coarse angular change (14 deg), to quickly solve even large launch angle problems.

    def outerLoop( theta: Double, da: Double ): Double = {
      def innerLoop( vx: Double, vy: Double, gx: Double, gy: Double, t: Double, dt: Double, x: Double, y: Double ): ( Double, Double, Double ) = {
        if ( vy < 0 && y < sightHeight / 12 ) {
          println( f"innerLoop exit 1: vy=$vy%3.6f is < 0 and y=$y%3.6f is < yIntercept=${sightHeight / 12}%3.6f" )
          ( t, x, y )
        }
        else if ( vy > 3 * vx ) {
          println( f"innerLoop exit 2: vy=$vy%3.6f is > 3*vx=3*$vx%3.6f" )
          ( t, x, y )
        }
        else if ( x >= 3 * zRange ) {
          println( f"innerLoop exit 3: x=$x%3.6f>=3*$zRange" )
          ( t, x, y )
        }
        else {
          var v = pow( pow( vx, 2 ) + pow( vy, 2 ), 0.5 )
          var dt = 0.5 / v
          var dv = retard( v )
          var dvy = -dv * vy / v * dt
          var dvx = -dv * vx / v * dt
          //println(f"		dvx=$dvx%3.6f,dvy=$dvy%3.6f")

          var vy1 = vy + dvy + dt * gy
          var vx1 = vx + dvx + dt * gx
          //			println(f"		vx=$vx%3.6f,vy=$vy%3.6f")
          //		    println("		dx=" + dt*(vx+vx1)/2 +", dy=" + dt*(vy+vy1)/2)
          //		    println(f"		vx1=$vx1%3.6f,vy1=$vy1%3.6f")
          //		    println(f"		gx=$gx%3.6f,gy=$gy%3.6f")
          //		    println(f"		t=$t%3.6f,dt=$dt%3.6f,x=$x%3.6f,y=$y%3.6f") //x in yds and y in inches
          //		    println
          innerLoop( vx1, vy1, gx, gy, t + 1 / v, 1 / v, x + dt * ( vx + vx1 ) / 2, y + dt * ( vy + vy1 ) / 2 )
        }
      }

      // If our accuracy is sufficient, or the angle is > 45 deg., we can stop approximating.
      if ( ( abs( da ) < MOAtoRad( 0.01 ) ) | ( theta > toRadians( 45 ) ) | theta < 0 ) {
        println( f"outerLoop Done: theta(rad) is $theta%3.6f, da is $da%3.6f, theta(deg) is: " + toDegrees( theta ) )
        println()
        theta
      }

      else {
        println( "*****************************************************************************" )
        println( f"outer loop: theta (rad): $theta%3.6f,da: $da%3.6f" )

        var ( t, x, y ) = innerLoop( vi * cos( theta ), vi * sin( theta ), Gravity * sin( theta ), Gravity * cos( theta ), 0, 0, 0, -sightHeight / 12 )

        println( f"innerLoop returns:t=$t%3.3f, x=$x%3.6f, y=$y%3.6f, " + pow( pow( y, 2 ) + pow( x - 3 * zRange, 2 ), .5 ) )

        if ( pow( pow( y, 2 ) + pow( x - 3 * zRange, 2 ), .5 ) <= 1e-4 ) { println( "Done!!!!" ); theta }
        else if ( y > 0 && da > 0 ) outerLoop( theta - da / 2, -da / 2 )
        else if ( y < 0 && da < 0 ) outerLoop( theta - da / 2, -da / 2 )
        else outerLoop( theta + da, da )
      }
    }

    toDegrees( outerLoop( toRadians( 0 ), toRadians( 1 ) ) ); // Convert to degrees for return value.
  }

  private def Windage(xx:Double, t:Double):Double ={
		val Vw = windSpeed.get*17.60 // Convert to inches per second.
		Vw*(t-xx/vi)
	  }

	  // Head wind is positive at WindAngle=0
  private def HeadWind:Double={
	    val Wangle = toRadians(windAngle.get)
	  	cos(Wangle)*windSpeed.get
	  }

	  // Positive is from Shooter's Right to Left (Wind from 90 degree)
  private def CrossWind:Double={
		val Wangle = toRadians(windAngle.get)
		sin(Wangle)*windSpeed.get
	  } 
 
  
  // Specialty angular conversion functions
  
  private def DegtoMOA(deg:Double):Double=deg*60
  private def DegtoRad(deg:Double):Double=deg*PI/180
  private def MOAtoDeg(moa:Double):Double=moa/60
  private def MOAtoRad(moa:Double):Double=moa/60*PI/180
  private def RadtoDeg(rad:Double):Double=rad*180/PI
  private def RadtoMOA(rad:Double):Double=rad*60*180/PI

  private def calcFRH:Double = {
	val VPw=4e-6*pow(temperature.get,3) - 0.0004*pow(temperature.get,2)+0.0234*temperature.get-0.2517
	val FRH=0.995*(pressure.get/(pressure.get-(0.3783)*(relHumidity.get)*VPw))
	FRH
  }

  private def calcFP:Double = {
	val Pstd=29.53 // in-hg
	val FP = (pressure.get-Pstd)/(Pstd)
	FP
}

  private def calcFT:Double = {
	val Tstd= -0.0036*altitude.get+59
	val FT = (temperature.get-Tstd)/(459.6+Tstd)
	FT
}

  private def calcFA:Double = {
	val fa= -4e-15*pow(altitude.get,3)+4e-10*pow(altitude.get,2)-3e-5*altitude.get+1
	(1/fa)
}

  private def AtmCorrect:Double={

	val FA = altitude match {case None => 1; case Some(_) => calcFA}
	val FT = temperature match {case None => 0; case Some(_) => calcFT}
	val FRH = relHumidity match {case None => 1; case Some(_) => calcFRH}
	val FP = pressure match {case None => 0; case Some(_) => calcFP}

	// Calculate the atmospheric correction factor
	val CD = (FA*(1+FT-FP)*FRH)
	//dragCoefficient*CD
	CD
  }
 }